//
//  MasterViewController.h
//  UED Notify
//
//  Created by minh phan on 11/28/14.
//  Copyright (c) 2014 phminh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RssParser.h"
#import "Haneke.h"

@interface MasterViewController : UITableViewController<NSXMLParserDelegate> {
//    int count;
//    NSXMLParser *parser;
//    NSMutableArray *feeds;
//    NSMutableDictionary *item;
//    NSMutableString *title;
//    NSMutableString *link;
//    NSMutableString *description;
//    NSString *element;
//    NSString *urlString;
}

@property (strong, nonatomic) RssParser *parse;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *barButton;



//-(void)startParse;
//
//-(int)getCount;

@end

