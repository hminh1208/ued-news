//
//  GlobalClass.m
//  UED Notify
//
//  Created by minh phan on 11/28/14.
//  Copyright (c) 2014 phminh. All rights reserved.
//

#import "GlobalClass.h"

@implementation GlobalClass

@synthesize urlLink;
@synthesize title;

static GlobalClass *instance = nil;

+(GlobalClass *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [GlobalClass new];
        }
    }
    return instance;
}

@end

