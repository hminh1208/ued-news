//
//  GlobalClass.h
//  UED Notify
//
//  Created by minh phan on 11/28/14.
//  Copyright (c) 2014 phminh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>

@interface GlobalClass : NSObject{
    NSString *urlLink;
    NSString *title;
}

@property(nonatomic,retain)NSString *urlLink;
@property(nonatomic,retain)NSString *title;

+(GlobalClass*)getInstance;

@end
