//
//  DetailViewController.m
//  UED Notify
//
//  Created by minh phan on 11/28/14.
//  Copyright (c) 2014 phminh. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()


@property (strong, nonatomic) IBOutlet UILabel *descLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

- (IBAction)chitietTapped:(id)sender;



@end

@implementation DetailViewController

@synthesize description;
@synthesize title;
@synthesize link;
@synthesize scrollView;
@synthesize contentView;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    _descLabel.text = description;
    _titleLabel.text = title;
}

//-(BOOL)shouldAutorotate{
//    return NO;
//}
//
//-(NSUInteger)supportedInterfaceOrientations{
//    return UIInterfaceOrientationPortrait;
//}

-(void)chitietTapped:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: link]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
