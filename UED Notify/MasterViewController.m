//
//  MasterViewController.m
//  UED Notify
//
//  Created by minh phan on 11/28/14.
//  Copyright (c) 2014 phminh. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "SWRevealViewController.h"
#import "GlobalClass.h"
#import "RssParser.h"
#import "MasterTableViewCell.h"

@interface MasterViewController ()

@property NSMutableArray *objects;
@end

@implementation MasterViewController

@synthesize parse;

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //set default value for global
    GlobalClass *obj=[GlobalClass getInstance];
    obj.urlLink = @"http://ued.vn/rss/tin-tieu-diem.rss";
    obj.title = @"Tiêu điểm";
    
    // GET INSTANCE SINGLETON OF RssParse
    parse = [RssParser getInstance];
    
    //create menu side button
    _barButton.target = self.revealViewController;
    _barButton.action = @selector(revealToggle:);
    
    //add gesture swipe left
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    //set title for view
    self.title = obj.title;
    
    //let start parse
    [parse startParse];
    
    //set notification receiver when change menu on SlideMenuTableViewController
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"myNotification"
                                               object:nil];
    
    // SET NOTIFICATION RECEIVER WHEN PARSE RSS DONE
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"reloadData"
                                               object:nil];
}

- (void)receiveNotification:(NSNotification *)notification
{
    // WHEN SIDE MENU BAR TAPPED
    if ([[notification name] isEqualToString:@"myNotification"]) {
        
        // CREATE THREAD TO START PARSE
        NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                     selector:@selector(start)
                                                       object:nil];
        
        // START PARSE
        [myThread start];
    }
    else if ([[notification name] isEqualToString:@"reloadData"]){
        // RELOAD THE TITLE
        GlobalClass *obj=[GlobalClass getInstance];
        self.title = obj.title;
        
        //RELOAD DATA FROM TABLEVIEW
        [self.tableView reloadData];
    }
}

// METHOD USE IN THREAD
-(void)start{
    [parse startParse];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        DetailViewController *controller = (DetailViewController *)segue.destinationViewController;
        controller.description = [[parse.feeds objectAtIndex:indexPath.row] objectForKey: @"description"];
        controller.title = [[parse.feeds objectAtIndex:indexPath.row] objectForKey: @"title"];
        controller.link = [[parse.feeds objectAtIndex:indexPath.row] objectForKey: @"link"];
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return parse.feeds.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"showDetail" sender:self];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *tableIdentifier = @"MasterCell";
    
    MasterTableViewCell *cell = (MasterTableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MasterCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.titleLabel.text = [[parse.feeds objectAtIndex:indexPath.row] objectForKey: @"title"];
    
    NSURL *imgUrl = [NSURL URLWithString:[[parse.feeds objectAtIndex:indexPath.row] objectForKey: @"image"]];
    
    HNKCacheFormat *format = [HNKCache sharedCache].formats[@"thumbnail"];

        format = [[HNKCacheFormat alloc] initWithName:@"thumbnail"];
        format.size = CGSizeMake(66, 74);
        format.scaleMode = HNKScaleModeFill;
        format.compressionQuality = 0.0;
        format.diskCapacity = 10 * 1024 * 1024; // 10MB
        format.preloadPolicy = HNKPreloadPolicyAll;
    
    
    cell.imageView.hnk_cacheFormat = format;
    
    [cell.imageView hnk_setImageFromURL:imgUrl];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}

@end
