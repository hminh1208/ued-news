//
//  MasterTableViewCell.m
//  UED Notify
//
//  Created by minh phan on 12/3/14.
//  Copyright (c) 2014 phminh. All rights reserved.
//

#import "MasterTableViewCell.h"

@implementation MasterTableViewCell

@synthesize titleLabel = _titleLabel;
@synthesize thumbailImageView = _thumbailImageView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
