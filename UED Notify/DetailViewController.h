//
//  DetailViewController.h
//  UED Notify
//
//  Created by minh phan on 11/28/14.
//  Copyright (c) 2014 phminh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *link;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIView *contentView;

@end

