//
//  MasterTableViewCell.h
//  UED Notify
//
//  Created by minh phan on 12/3/14.
//  Copyright (c) 2014 phminh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIImageView *thumbailImageView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@end
