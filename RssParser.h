//
//  RssParser.h
//  UED Notify
//
//  Created by minh phan on 12/3/14.
//  Copyright (c) 2014 phminh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GlobalClass.h"

@interface RssParser : NSObject<NSXMLParserDelegate>{

    int count;
    NSXMLParser *parser;
    NSMutableArray *feeds;
    NSMutableDictionary *item;
    NSMutableString *title;
    NSMutableString *link;
    NSMutableString *description;
    NSMutableString *image;
    NSString *element;
    NSString *urlString;
    NSString *imageString;
}

@property (nonatomic,readwrite) int *_count;
@property (nonatomic,retain) NSMutableArray *feeds;

+(RssParser*)getInstance;

-(void)startParse;

@end
