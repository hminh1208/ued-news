//
//  RssParser.m
//  UED Notify
//
//  Created by minh phan on 12/3/14.
//  Copyright (c) 2014 phminh. All rights reserved.
//

#import "RssParser.h"

@implementation RssParser

@synthesize feeds;

static RssParser *instance = nil;

+(RssParser*)getInstance{
    
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [RssParser new];
        }
    }
    
    return instance;
}

- (void)startParse{
    
    if (feeds) {
        [feeds removeAllObjects];
    }else{
        feeds = [[NSMutableArray alloc] init];
    }
    
    GlobalClass *obj=[GlobalClass getInstance];
    NSURL *url = [NSURL URLWithString:obj.urlLink];
    
    parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    
    [parser setDelegate:self];
    
    [parser parse];
    
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict;
{
    
    element = elementName;
    
    if ([element isEqualToString:@"item"]) {
        
        item    = [[NSMutableDictionary alloc] init];
        title   = [[NSMutableString alloc] init];
        link    = [[NSMutableString alloc] init];
        description    = [[NSMutableString alloc] init];
        image = [[NSMutableString alloc] init];
        
        count++;
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName;
{
    if ([elementName isEqualToString:@"item"]) {
        
        [item setObject:title forKey:@"title"];
        [item setObject:link forKey:@"link"];
        [item setObject:description forKey:@"description"];
        [item setObject:image forKey:@"image"];
        
        [feeds addObject:[item copy]];
        
    }else if ([elementName isEqualToString:@"channel"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string;
{
    if ([element isEqualToString:@"title"]) {
        [title appendString:string];
    } else if ([element isEqualToString:@"link"]) {
        [link appendString:string];
    }else if ([element isEqualToString:@"content"]){
        [description appendString:string];
    }else if ([element isEqualToString:@"thumb"]){
        [image appendString:string];
    }
}


@end
